/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import liquid.*;

public class IcedCocktailRecipe extends CocktailRecipe {

	private float ice;
	
	/* Vorbedingung : name != null, ice >= 0, liquids != null, liquids enthält
	 * eine oder mehr Flüssigkeiten.
	 * Nachbedingung: Erzeugt ein neues Rezept für einen mit Crush-Ice
	 * gekühlten Cocktail names name, mit der Eismenge ice und den Zutaten
	 * liquids.
	 */
	public IcedCocktailRecipe(String name, float ice, Liquid ...liquids) {
		super(name, liquids);
		this.ice = ice;
	}
	
	/* Nachbedingung: Liefert die Menge Eis die für die Zubereitung dieses
	 * Rezepts benötigt wird.
	 */
	public float getIce() {
		return ice;
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Versucht den dem Rezept entsprechenden Cocktail durch
	 * den Cocktailroboter robot zubereiten zu lassen. Falls das Rezept nicht
	 * zubereitet werden kann wird eine Exception geworfen.
	 */
	@Override
	public Cocktail makePrepareBy(CocktailRobot robot)
			throws CocktailRobotException {
		return robot.prepare(this);
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Liefert true falls das Rezept durch den Roboter robot
	 * zubereitet werden kann.
	 */
	@Override
	public boolean preparedBy(CocktailRobot robot) {
		return robot.canPrepare(this);
	}
	
	/* Nachbedingung: Liefert eine String-Darstellung des Rezeptes in der Form:
	 * Rezept, Crush Ice (XXXX.Yg)
	 */
	@Override
	public String toString() {
		return super.toString()
				+ ", Crush Ice (" + String.format("%4.1f", ice) + " g)";
	}
}
