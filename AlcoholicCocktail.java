/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;
import java.util.LinkedList;

public class AlcoholicCocktail implements Cocktail {

	private String name;
	private int volume;
	private int alcohol;
	private Collection<String> liquidNames;
	
	/* Vorbedingung : name != 0, volume >= 0, alcohol >= 0, liquidNames != null
	 * Nachbedingung: Erzeugt ein neues AlcoholicCocktail Objekt
	 * dessen mit dem namen name, dem gesamtvolumen volume, dem alcoholgehalt
	 * alcohol und den namen der enthaltenen Flüssigkeiten.
	 */
	public AlcoholicCocktail(String name, int volume, int alcohol,
			Collection<String> liquidNames) {
		this.name = name;
		this.volume = volume;
		this.alcohol = alcohol;
		this.liquidNames = new LinkedList<String>(liquidNames);
	}
	
	/* Nachbedingung: Liefert eine String Darstellung in der Form:
	 * name (xmL, alcohol y%){, liquid}
	 */
	@Override
	public String toString() {
		String result = name
				+ " (" + volume + "mL, alcohol " + alcohol + "%) : ";
		
		for (String liquid : liquidNames) {
			result += liquid + ", ";
		}
		
		if (0 != liquidNames.size()) {
			result = result.substring(0, result.length() - 2);
		}
		
		return result;
	}
}
