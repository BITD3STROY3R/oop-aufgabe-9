/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;

import liquid.*;

public class Test {
	
	private static final int pageWidth   = 45;
	private static final char headerChar = '=';
	private static final char sectionChar = '-';
	private static final int headerWidth = 45;
	private static final int labelMargin = 4;
	
	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert einen String bestehend aus der N-Fachen
	 * wiederholung des Zeichens c.
	 */
	private static String repeat(int n, char c) {
		String result = "";

		for (int i = 0; i < n; i++) {
			result += c;
		}

		return result;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String der maximal headerWidth Zeichen
	 * lang ist. Der String beginnt und endet mit maximal labelMargin vielen
	 * Leerzeichen. Die Anzahl der nachfolgenden Leerzeichen ist immer gleich
	 * der der Fuehrenden.
	 */
	private static String makeLabel(String label) {
		if (label.length() <= headerWidth) {
			int i = 0;
			while (i < labelMargin && label.length() <= headerWidth - 2) {
				label = " " + label + " ";
				i++;
			}
		} else {
			label = label.substring(0, headerWidth);
		}

		return label;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String welcher eine formatierte
	 * Ueberschrift darstellt. Label ist der Text der Ueberschrift.
	 */
	private static String makeHeader(String label) {
		String header = "";

		header += repeat(headerWidth, headerChar) + '\n';
		label = makeLabel(label);
		header += repeat((headerWidth - label.length()) / 2, headerChar);
		header += label;
		header += repeat((headerWidth - label.length()) / 2
				+ (headerWidth - label.length()) % 2, headerChar);
		header += '\n';
		header += repeat(headerWidth, headerChar);
		header += "\n";
		
		return header;
	}

	/* Nachbedingung: Liefert einen String welcher eine Abtrennungszeile
	 * darstellt. Diese Zeile hat eine Breite von genau pageWidth zeichen.
	 */
	private static String makeSectionbar() {
		return repeat(pageWidth, sectionChar);
	}
	
	private static CocktailMenu addDefaultCocktailRecipes(CocktailMenu menu){
		menu.addCocktail(
				new RegularCocktailRecipe("Cola mit Rum", new Rhum(40), new Cola(210)));
		menu.addCocktail(
				new IcedCocktailRecipe("Long Island Iced Tea", 100, new Vodka(15), new Tequila(15), new Rhum(15), new TripleSec(15), new Gin(15), new Cola(15)));
		menu.addCocktail(
				new IcedCocktailRecipe("Long Island Iced Tea light", 100, new Vodka(15), new Tequila(15), new Cola(15)));
		menu.addCocktail(
				new HotCocktailRecipe("Heisse Schokolade mit Rum", 100, new Rhum(40), new HotChocolate(210)));
		
		return menu;
	}
	
	private static void printOrder(Collection<CocktailRecipe> o){
		String s = "";
		
		for (CocktailRecipe r : o){
			s += r.getName()+", ";
		}
		
		System.out.println(s.substring(0, s.length()-2));;
		
	}

	public static void main(String[] args) {
		test1();
		test2();
		test3();
		
		System.out.println(makeHeader("Object Count"));
	}
	
	private static void test1(){
		System.out.println(makeHeader("TEST 1"));
		System.out.println("(testing menus) - Menu for all tests:");
		System.out.println(makeSectionbar());
		
		CocktailMenu menu = addDefaultCocktailRecipes(new CocktailMenu());
		menu.printMenu();
		
		System.out.println();
		System.out.println();
		
	}
	
	private static void test2(){
		System.out.println(makeHeader("TEST 2"));
		System.out.println("testing orders, trays, bars (including robots - Long Island cannot be made (more than 3 ingredients!) // last order cannot be fully made because they run out of Cola )");
		System.out.println(makeSectionbar());
		
		CocktailMenu menu = addDefaultCocktailRecipes(new CocktailMenu());
		
		CocktailBar bar = new CocktailBar();	
		
		CocktailOrder order1 = new CocktailOrder();	
		order1.addOrder(menu.findCocktail("Cola mit Rum"));
		order1.addOrder(menu.findCocktail("Cola mit Rum"));
		System.out.println("Order 1");
		printOrder(order1.list());
		
		CocktailOrder order2 = new CocktailOrder();	
		order2.addOrder(menu.findCocktail("Cola mit Rum"));
		System.out.println("Order 2");
		printOrder(order2.list());
		
		CocktailOrder order3 = new CocktailOrder();	
		order3.addOrder(menu.findCocktail("Heisse Schokolade mit Rum"));
		order3.addOrder(menu.findCocktail("Heisse Schokolade mit Rum"));
		System.out.println("Order 3");
		printOrder(order3.list());
		
		CocktailOrder order4 = new CocktailOrder();	
		order4.addOrder(menu.findCocktail("Cola mit Rum"));
		order4.addOrder(menu.findCocktail("Cola mit Rum"));
		order4.addOrder(menu.findCocktail("Heisse Schokolade mit Rum"));
		order4.addOrder(menu.findCocktail("Long Island Iced Tea"));
		System.out.println("Order 4");
		printOrder(order4.list());
		
		CocktailOrder order5 = new CocktailOrder();	
		order5.addOrder(menu.findCocktail("Cola mit Rum"));
		order5.addOrder(menu.findCocktail("Cola mit Rum"));
		order5.addOrder(menu.findCocktail("Cola mit Rum"));
		System.out.println("Order 5");
		printOrder(order5.list());
		
		System.out.println(makeSectionbar());
		
		CocktailTray tray1 = bar.prepareOrder(order1);
		CocktailTray tray2 = bar.prepareOrder(order2);
		CocktailTray tray3 = bar.prepareOrder(order3);
		CocktailTray tray4 = bar.prepareOrder(order4);
		CocktailTray tray5 = bar.prepareOrder(order5);
		System.out.println(makeSectionbar());
		
		System.out.println("Tray 1 (2x Cola mit Rum)");
		tray1.content();	
		System.out.println(makeSectionbar());
		
		System.out.println("Tray 2 (Cola mit Rum)");
		tray2.content();	
		System.out.println(makeSectionbar());
		
		System.out.println("Tray 3 (2x Heisse Schokolade mit Rum)");
		tray3.content();
		System.out.println(makeSectionbar());
		
		System.out.println("Tray 4 (2x Cola mit Rum & Heisse Schokolade mit Rum)");
		tray4.content();
		System.out.println(makeSectionbar());
		
		System.out.println("Tray 5 (nichts, da kein Cola mehr vorhanden)");
		tray5.content();
		
		System.out.println();
		System.out.println();
	}
	
	private static void test3(){
		System.out.println(makeHeader("TEST 3"));
		System.out.println("testing robots separately");
		System.out.println(makeSectionbar());
		
		
		System.out.println("using up all the Vodka");
		CocktailRobot cr = new CoolingCocktailRobot(200);
		cr.addLiquid(new Vodka(30));
		cr.addLiquid(new Tequila(45));
		cr.addLiquid(new Cola(45));
		
		IcedCocktailRecipe recipe1 = new IcedCocktailRecipe("Long Island Iced Tea light", 100, new Vodka(15), new Tequila(15), new Cola(15));
		
		try {
			cr.prepare(recipe1);
			cr.prepare(recipe1);
			cr.prepare(recipe1);
		} catch (CocktailRobotException e) {
			System.out.println("EXCEPTION: " + e.getMessage());
		}
		System.out.println(makeSectionbar());
		
		// -----------------------
		
		System.out.println("trying to prepare a Cooled Cocktail on a Regular CocktailRobot");
		CocktailRobot rr = new CocktailRobot();
		rr.addLiquid(new Vodka(15));
		rr.addLiquid(new Tequila(15));
		rr.addLiquid(new Cola(15));
		
		try {
			rr.prepare(recipe1);
		} catch (CocktailRobotException e) {
			System.out.println("EXCEPTION: " + e.getMessage());
		}
		System.out.println(makeSectionbar());
		
		// -----------------------
		
		System.out.println("too many ingredients");
		CocktailRobot cr2 = new CoolingCocktailRobot(200);
		cr2.addLiquid(new Vodka(15));
		cr2.addLiquid(new Tequila(15));
		cr2.addLiquid(new Cola(15));
		cr2.addLiquid(new Rhum(15));
		
		IcedCocktailRecipe recipe2 = new IcedCocktailRecipe("Long Island Iced Tea light", 100, new Vodka(15), new Tequila(15), new Cola(15), new Rhum(15));
		
		try {
			cr2.prepare(recipe2);
		} catch (CocktailRobotException e) {
			System.out.println("EXCEPTION: " + e.getMessage());
		}
		System.out.println(makeSectionbar());
		
		System.out.println();
		System.out.println();
	}
}
