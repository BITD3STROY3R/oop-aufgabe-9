/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public aspect ObjectCounter {

	private int objectCounter = 0;
	
	/* Umfasst alle Aufrufe von Konstruktoren, die nicht innherhalb des
	 * Aspektes ObjectCounter liegen.
	 */
	private pointcut constructor() : call(*.new(..)) && !within(ObjectCounter);
	
	/* Umfasst alle Aufrufe einer Methode namens main mit der Rückgabe void und
	 * einem einzigen Argument vom Typ String[].
	 */
	private pointcut main() : execution(void *.main(String[]));
	
	/* Erhöht objectCounter um eins */
	after() : constructor() {
		objectCounter++;
	}
	
	/* Gibt den aktuellen wert von objectCounter am Bildschirm aus */
	after() : main() {
		System.out.println(objectCounter + " Objects were constructed");
	}
}
