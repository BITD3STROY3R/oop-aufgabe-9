/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;

public class CocktailOrder {

	private Collection<CocktailRecipe> orders
			= new LinkedList<CocktailRecipe>();
	
	/* Vorbedingung : order != null
	 * Nachbedingung: Fügt die das Cocktailrezept order zur liste der 4
	 * Bestellungen hinzu.
	 */
	public void addOrder(CocktailRecipe order) {
		orders.add(order);
	}
	
	/* Nachbedingung: Liefert die Liste der rezepte der bestellten Cocktails
	 * dieser Bestellung.
	 */
	public Collection<CocktailRecipe> list() {
		return orders;
	}
}
