/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;

import liquid.*;

public class CocktailBar {

	private Collection<CocktailRobot> robots = new LinkedList<CocktailRobot>();
	
	/* Nachbedingung: Erzeugt ein neues CocktailBar Objekt */
	public CocktailBar() {
		robots.add(new CoolingCocktailRobot(1000));
		robots.add(new HeatingCocktailRobot());
		robots.add(new CocktailRobot());
		
		for (CocktailRobot c : robots) {
			c.addLiquid(new Vodka(1500));
			c.addLiquid(new Rhum(1500));
			c.addLiquid(new HotChocolate(1500));
			c.addLiquid(new Cola(1500));
			c.addLiquid(new TripleSec(1500));
			c.addLiquid(new Tequila(1500));
			c.addLiquid(new Gin(1500));
		}
	}
	
	/* Vorbedingung : order != null
	 * Nachbedingung: Bereitet die Bestellung order zu, un liefert ein
	 * Tablett, das die erzeugten Cocktails enthält. Falls ein Cocktail nicht
	 * erzeugt werden konnte, wird eine Fehlermeldung am Bildschirm ausgegeben.
	 */
	public CocktailTray prepareOrder(CocktailOrder order) {
		
		CocktailTray tray = new CocktailTray();
		
		for (CocktailRecipe recipe : order.list()) {
			Iterator<CocktailRobot> it = robots.iterator();
			boolean prepared = false;
			
			try {
				while (it.hasNext() && !prepared) {
					CocktailRobot robot = it.next();
					
					if (recipe.preparedBy(robot)) {
						tray.addCocktail(recipe.makePrepareBy(robot));
						prepared = true;
					}
				}
				if (!prepared) {
					System.out.println("Cannot prepare Cocktail");
				}
			} catch (CocktailRobotException e) {
				System.out.println(e.getMessage());
			}
		}
		
		return tray;
	}
}
