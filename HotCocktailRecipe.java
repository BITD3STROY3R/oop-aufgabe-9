/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import liquid.*;

public class HotCocktailRecipe extends CocktailRecipe {

	private int temperature;
	
	/* Vorbedingung : name != null, temperature >= 0, liquids != null, liquids
	 * enthält eine oder mehr Flüssigkeiten.
	 * Nachbedingung: Erzeugt ein neues Rezept für einen heißen Cocktail namens
	 * name, mit der Temperatur temperature und den Zutaten liquids.
	 */
	public HotCocktailRecipe(String name, int temperature, Liquid ...liquids) {
		super(name, liquids);
		this.temperature = temperature;
	}
	
	/* Nachbedingung: Liefert die Temperatur. */
	public int getTemperature() {
		return temperature;
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Bereitet dieses Rezept durch den Roboter robot zu. Falls
	 * dieses Rezept nicht von robot zubereitet werden kann, wird eine
	 * Exception geworfen.
	 */
	@Override
	public Cocktail makePrepareBy(CocktailRobot robot)
			throws CocktailRobotException {
		return robot.prepare(this);
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Liefert true falls der Cocktailroboter robot dieses
	 * Rezept zubereiten kann.
	 */
	@Override
	public boolean preparedBy(CocktailRobot robot) {
		return robot.canPrepare(this);
	}
	
	/* Nachbedingung: Liefert eine String-Darstellung des Rezeptes in der Form:
	 * Rezept, Temperature (X°C)
	 */
	@Override
	public String toString() {
		return super.toString() + ", Temperature (" + temperature + " °C)";
	}
}
