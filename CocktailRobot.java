/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;
import java.util.ArrayList;
import liquid.*;

public class CocktailRobot {
	
	private static final int maxLiquids = 3;
	private ArrayList<Liquid> liquids = new ArrayList<Liquid>();
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Bereitet den dem Rezept recipe entsprechenden Cocktail
	 * zu. Falls der Cocktail nicht zubereitet werden konnte wird eine
	 * Exception geworfen.
	 */
	protected final Cocktail make(CocktailRecipe recipe)
			throws CocktailRobotException {
		
		Collection<Liquid> required = recipe.getLiquids();
		
		if (required.size() > maxLiquids) {
			throw new CocktailRobotException("More than " + maxLiquids
					+ " required Liquids");
		}
		
		for (Liquid l : required) {
			if (!liquids.contains(l)) {
				throw new CocktailRobotException("No such liquid : " + l);
			}
			Liquid storedLiquid = liquids.get(liquids.indexOf(l));
			
			if (storedLiquid.getVolume() < l.getVolume()) {
				throw new CocktailRobotException("Liquid "
						+ storedLiquid.getName() + " consumed");
			}
			
			storedLiquid.decreaseVolume(l.getVolume());
		}
		
		int volume  = computeVolume(required);
		int alcohol = computeAlcohol(required);
		
		Cocktail cocktail;
		
		if (alcohol > 0) {
			cocktail = new AlcoholicCocktail(recipe.getName(), volume, alcohol,
					liquidNames(required));
		} else {
			cocktail = new RegularCocktail(recipe.getName(), volume,
					liquidNames(required));
		}
		
		return cocktail;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Bereitet den dem Rezept recipe entsprechenden Cocktail
	 * zu. Falls der Cocktail nicht zubereitet werden konnte wird eine
	 * Exception geworfen.
	 */
	public Cocktail prepare(RegularCocktailRecipe recipe) 
			throws CocktailRobotException {
		return make(recipe);
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Bereitet den dem Rezept recipe entsprechenden Cocktail
	 * zu. Falls der Cocktail nicht zubereitet werden konnte wird eine
	 * Exception geworfen.
	 */
	public Cocktail prepare(IcedCocktailRecipe recipe)
			throws CocktailRobotException {
		throw new CocktailRobotException("Cannot prepare recipe");
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Bereitet den dem Rezept recipe entsprechenden Cocktail
	 * zu. Falls der Cocktail nicht zubereitet werden konnte wird eine
	 * Exception geworfen.
	 */
	public Cocktail prepare(HotCocktailRecipe recipe)
			throws CocktailRobotException {
		throw new CocktailRobotException("Cannot prepare recipe");
	}
	
	/* Vorbedingung : liquid != null
	 * Nachbedingung: Fügt zur Reserve des Roboters die Flüssigkeit liquid
	 * hinzu. Existiert diese Flüssigkeit bereits im Roboter, so wird ihr
	 * Volumen um das von Liquid erhöht.
	 */
	public CocktailRobot addLiquid(Liquid liquid) {
		if (liquids.contains(liquid)) {
			liquids.get(liquids.indexOf(liquid))
				.increaseVolume(liquid.getVolume());
		} else {
			liquids.add(liquid);
		}
		return this;
	}
	
	/* Vorbedingung : liquids != null
	 * Nachbedingung: Berechnet den Alkoholgehalt der Flüssigkeiten liquids
	 * als gewichtete summe der einzelnen Alkoholgehalte.
	 */
	private int computeAlcohol(Collection<Liquid> liquids) {
		float sumAlcohol = 0;
		int sumVolume = 0;
		
		for (Liquid l : liquids) {
			sumAlcohol += l.getAlcohol() * l.getVolume();
			sumVolume  += l.getVolume();
		}
		
		return (int) Math.ceil(sumAlcohol / sumVolume);
	}
	
	/* Vorbedingung : liquids != null
	 * Nachbedingung: Liefert die Summe der Volumen der Flüssigkeiten liquids.
	 */
	private int computeVolume(Collection<Liquid> liquids) {
		int sumVolume = 0;
		
		for (Liquid l : liquids) {
			sumVolume += l.getVolume();
		}
		
		return sumVolume;
	}
	
	/* Vorbedingung : liquids != null
	 * Nachbedingung: Liefert die Menge der Namen der Flüssigkeiten liquids.
	 */
	private Collection<String> liquidNames(Collection<Liquid> liquids) {
		LinkedList<String> names = new LinkedList<String>();
		
		for (Liquid l : liquids) {
			if (!names.contains(l.getName())) {
				names.add(l.getName());
			}
		}
		
		return names;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer true.
	 */
	public boolean canPrepare(RegularCocktailRecipe recipe) {
		return true;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer false.
	 */
	public boolean canPrepare(IcedCocktailRecipe recipe) {
		return false;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer false.
	 */
	public boolean canPrepare(HotCocktailRecipe recipe) {
		return false;
	}

}
