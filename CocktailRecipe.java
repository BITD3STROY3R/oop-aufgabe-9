/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;

import liquid.*;

public abstract class CocktailRecipe {

	private LinkedList<Liquid> liquids = new LinkedList<Liquid>();
	private String name;
	
	/* Vorbedingung : name != null, liquids != null, liquids enthält eine oder
	 * mehrere Flüssigkeiten.
	 * Nachbedingung: Erzeugt ein neues CocktailRecipe Objekt mit dem namen
	 * name und den Zutaten liquids.
	 */
	public CocktailRecipe(String name, Liquid ... liquids) {
		this.name = name;
		
		for (Liquid l : liquids) {
			this.liquids.addLast(l);
		}
	}
	
	/* Nachbedingung: Liefert den Namen des Cocktailrezeptes. */
	public String getName() {
		return name;
	}
	
	/* Nachbedingung: Liefert eine Liste der Flüssigkeiten die als Zutat für
	 * dieses Rezept benötigt werden.
	 */
	public Collection<Liquid> getLiquids() {
		return new LinkedList<Liquid>(liquids);
	}
	
	/* Nachbedingung: Liefert eine String-Darstellung des Rezepts in der Form:
	 * Name des Rezepts: Flüssigkeit {, Flüssigkeit}
	 */
	@Override
	public String toString() {
		String result = name + ": ";
		
		for (Liquid l : liquids) {
			result += l.toString() + ", ";
		}
		
		return result.substring(0, result.length() - 2);
	}
	
	/* Vorbedingung : cr != null
	 * Nachbedingung: Bereitet den dem Rezept entsprechenden Cocktail zu. Falls
	 * der Cocktail nicht von dem Roboter cr zubereitet werden kann wird eine
	 * Exception geworfen.
	 */
	public abstract Cocktail makePrepareBy(CocktailRobot cr)
			throws CocktailRobotException;
	
	/* Vorbedingung : cr != null
	 * Nachbedingung: Liefert true falls der Cocktailroboter cr das Rezept
	 * zubereiten kann.
	 */
	public abstract boolean preparedBy(CocktailRobot cr);
}
