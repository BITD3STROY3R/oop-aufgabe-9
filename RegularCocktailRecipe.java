/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import liquid.*;

public class RegularCocktailRecipe extends CocktailRecipe {

	/* Vorbedingung : name != null, liquids != null, liquids enthält eine oder
	 * mehr Flüssigkeiten.
	 * Nachbedingung: Erzeugt ein neues Cocktailrezept, für einen Cocktail
	 * namens Name mit den Zutaten liquids.
	 */
	public RegularCocktailRecipe(String name, Liquid ... liquids) {
		super(name, liquids);
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Versucht den durch dieses Rezept beschriebenen Cocktail
	 * durch den Cocktailroboter robot zubereiten zu lassen. Falls dies nicht
	 * gelingt, wird eine Exception geworfen.
	 */
	@Override
	public Cocktail makePrepareBy(CocktailRobot robot)
		throws CocktailRobotException {
		return robot.prepare(this);
	}
	
	/* Vorbedingung : robot != null
	 * Nachbedingung: Liefert true falls der Cocktailroboter robot dieses 
	 * Rezept, ungeachtet der verbleibenden Flüssigkeiten, zubereiten kann. 
	 */
	@Override
	public boolean preparedBy(CocktailRobot robot) {
		return robot.canPrepare(this);
	}
}
