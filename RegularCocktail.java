/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;

public class RegularCocktail implements Cocktail {

	private String name;
	private int volume;
	private Collection<String> liquidNames;
	
	/* Vorbedingung : name != null, volume >= 0, liquidNames != null, 
	 * liquidNames.size() >= 1
	 * Nachbedingung: Erzeugt einen neuen Cocktail mit dem Namen name, dem
	 * Gesamtvolumen volume und liquidNames als Namen der Zutaten.
	 */
	public RegularCocktail(String name, int volume,
			Collection<String> liquidsNames) {
		this.name = name;
		this.volume = volume;
		this.liquidNames = new LinkedList<String>(liquidNames);
	}
	
	/* Nachbedingung: Liefert eine String-Darstellung des Cocktails der Form:
	 * name (volume mL), liquid {, liquid}
	 */
	@Override
	public String toString() {
		String result = name + "(" + volume + "mL) : ";
		
		for (String liquid : liquidNames) {
			result += liquid + ", ";
		}
		
		if (0 != liquidNames.size()) {
			result = result.substring(0, result.length() - 2);
		}
		return result;
	}
}
