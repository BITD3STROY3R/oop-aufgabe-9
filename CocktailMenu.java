/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.HashMap;

public class CocktailMenu {
	
	private HashMap<String, CocktailRecipe> cocktails
			= new HashMap<String, CocktailRecipe>();
	
	/* Vorbedingung : cocktail != null
	 * Nachbedingung: Fügt das Cocktailrezept cocktail zu der Cocktailkarte
	 * hinzu.
	 */
	public CocktailMenu addCocktail(CocktailRecipe cocktail) {
		cocktails.put(cocktail.getName(), cocktail);
		
		return this;
	}
	
	/* Nachbedingung: Liefert das Cocktailrezept dessen name gleich name ist.
	 * Falls ein solches Rezept nicht gefunden werden konnte wird null
	 * zurückgegeben.
	 */
	public CocktailRecipe findCocktail(String name) {
		return cocktails.get(name);
	}
	
	/* Nachbedingung: Gibt zeilenweise alle sich in der Liste befindlichen 
	 * Cocktailrezepte am Bildschrim aus.
	 */
	public void printMenu() {
		System.out.println("Cocktail Menu");
		
		for (CocktailRecipe recipe : cocktails.values()) {
			System.out.println("\t" + recipe);
		}
	}
}
