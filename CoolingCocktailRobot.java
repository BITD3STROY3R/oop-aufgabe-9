/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class CoolingCocktailRobot extends CocktailRobot {
	
	private float ice;
	
	/* Vorbedingung : ice >= 0
	 * Nachbedingung: Erzeugt einen neuen Cocktailroboter, welcher über ice
	 * Gramm Crush-Ice verfügt.
	 */
	public CoolingCocktailRobot(float ice) {
		this.ice = ice;
	}

	/* Vorbedingung : recipe != null
	 * Nachbedingung: Bereitet den dem Rezept recipe entsprechenden Cocktail
	 * mit der in recipe spezifizierten Menge Eis zu.
	 */
	@Override
	public Cocktail prepare(IcedCocktailRecipe recipe)
			throws CocktailRobotException {
		if (recipe.getIce() > ice) {
			throw new CocktailRobotException("Ice consumed");
		}
		return new IcyCocktail(make(recipe), recipe.getIce());
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Wirft immer eine Exception.
	 */
	@Override
	public Cocktail prepare(RegularCocktailRecipe recipe)
			throws CocktailRobotException {
		throw new CocktailRobotException("Cannot prepare recipe");
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Wirft immer eine Exception.
	 */
	@Override
	public Cocktail prepare(HotCocktailRecipe recipe)
			throws CocktailRobotException {
		throw new CocktailRobotException("Cannot prepare recipe");
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer false.
	 */
	@Override
	public boolean canPrepare(RegularCocktailRecipe recipe) {
		return false;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer true.
	 */
	@Override	
	public boolean canPrepare(IcedCocktailRecipe recipe) {
		return true;
	}
	
	/* Vorbedingung : recipe != null
	 * Nachbedingung: Liefert immer false.
	 */
	@Override
	public boolean canPrepare(HotCocktailRecipe recipe) {
		return false;
	}
}
