/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 9 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class IcyCocktail implements Cocktail {
	
	private Cocktail cocktail;
	private float ice;
	
	/* Vorbedingung : cocktail != null, ice >= 0
	 * Nachbedingung: Erzeugt ein neues Objekt das die Zusammensetzung des
	 * Cocktails cocktail mit der Menge Eis ice darstellt.
	 */
	public IcyCocktail(Cocktail cocktail, float ice) {
		this.cocktail = cocktail;
		this.ice      = ice;
	}
	
	/* Nachbedingung: Liefert eine String-Darstellung des Eis-gekühlten
	 * Cocktails in der Form: cocktail, Crush Ice (XXXX.Yg)
	 */
	public String toString() {
		return cocktail + ", Crush Ice (" + String.format("%4.1f", ice) + "g)";
	}
}
